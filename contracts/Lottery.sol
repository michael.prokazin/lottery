// SPDX-License-Identifier: MIT

pragma solidity ^0.8.6;

contract Lottery {
    address public owner = msg.sender;
    address payable[] public players;
    uint256 currentRandom = random(0, 0);

    function enter(uint32 someNumber) public payable {
        // TODO: Entrance value affects win chance.
        require(msg.value > .001 ether, "Not enough value.");
        players.push(payable(msg.sender));
        currentRandom = random(currentRandom, someNumber);
    }

    function random(uint256 currentNumber, uint32 someNumber) public view returns (uint256) {
        return uint256(keccak256(abi.encode(someNumber, block.timestamp, msg.sender, currentNumber)));
    }

    function raffle() public {
        require(msg.sender == owner);
        require(players.length > 0, "No players in current game.");
        uint256 index = currentRandom % players.length;
        players[index].transfer(address(this).balance);
    }
}
